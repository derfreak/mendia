FROM ubuntu:25.04

# Install system packages
RUN apt update && apt -y install curl gnupg python3 python3-pip libmysqlclient-dev git libssl-dev librust-openssl-sys-dev ca-certificates

# Install nodejs
RUN curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg
ENV NODE_MAJOR=22
RUN echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_$NODE_MAJOR.x nodistro main" | tee /etc/apt/sources.list.d/nodesource.list
RUN apt update && apt install -y nodejs

# Install rust toolchain
RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y
ENV PATH="/root/.cargo/bin:${PATH}"

RUN rustup component add rustfmt &&\
    rustup component add clippy &&\
    cargo install diesel_cli --no-default-features --features mysql &&\
    cargo install cargo-deb &&\
    cargo install cargo-edit

# Install python dependencies
COPY requirements.txt /requirements/
RUN pip install -r requirements/requirements.txt --break-system-packages

# Install semantic release
RUN npm install --global semantic-release &&\
    npm install --global semantic-release/gitlab &&\
    npm install --global semantic-release/changelog &&\
    npm install --global semantic-release/git &&\
    npm install --global semantic-release/exec
