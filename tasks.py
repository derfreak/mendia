import os
from invoke import task
from pathlib import Path
import shutil
import colorama

# Initialize colorama
colorama.init(autoreset=True)
root_directory = Path(".").absolute()
pty = os.name != 'nt'

@task
def db(context, host="127.0.0.1", port=6677, database="mendia", user="root", password="example"):
    with context.cd("backend/mendia"):
        context.run(
            f"diesel setup --database-url mysql://{user}:{password}@{host}:{port}/{database}",
            encoding="utf-8",
            pty=pty
        )
        context.run(
            f"diesel migration --database-url mysql://{user}:{password}@{host}:{port}/{database} run",
            encoding="utf-8",
            pty=pty
        )

@task
def install_npm_dependencies(context):
    with context.cd("frontend"):
        context.run("npm install", encoding="utf-8", pty=pty)

@task
def build_api(context):
    with context.cd("backend"):
        context.run("cargo test -p mendia", encoding="utf-8", pty=pty)
    path = root_directory / "backend" / "mendia" / "bindings"
    target_directory = root_directory / "frontend" / "src" / "lib" / "api"

    for file in target_directory.glob('*.ts'):
        file.unlink()

    for file in path.glob('*.ts'):
        shutil.copy(file, target_directory / file.name)

@task(build_api, install_npm_dependencies)
def build_frontend(context):
    with context.cd("frontend"):
        context.run("npm run build", encoding="utf-8", pty=pty)

@task()
def docker(context):
    with context.cd(root_directory / "docker"):
        context.run("docker compose build --pull", encoding="utf-8", pty=pty)
        context.run("docker compose stop", encoding="utf-8", pty=pty)
        context.run("docker compose up -d", encoding="utf-8", pty=pty)

@task(build_api, install_npm_dependencies)
def frontend(context):
    with context.cd("frontend"):
        context.run(
            "npm run dev",
            encoding="utf-8",
            pty=pty,
        )

@task
def backend(context):
    db(context)
    with context.cd("backend"):
        context.run(
            "cargo run --bin mendia -- --config ./config.yml",
            encoding="utf-8",
            pty=pty,
        )
