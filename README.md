# Mendia

This project bundles a [frontend](frontend/README.md) and a [backend](backend/mendia/README.md).

## Dependencies on Windows

Use an elevated powershell for the commands in the Windows section.
Elevated means that the shell must have administrator rights.

### Enable WSL2

```powershell
dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart
dism.exe /online /enable-feature /featurename:VirtualMachinePlatform /all /norestart
```

Download & Install https://wslstorestorage.blob.core.windows.net/wslblob/wsl_update_x64.msi

```powershell
wsl --set-default-version 2
```

Do a restart.

### Chocolatey

```powershell
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))
```

```powershell
choco install vscode rust nodejs docker-desktop git mysql python
```

## Installation

```console
git clone https://gitlab.com/derfreak/mendia.git
cd mendia
rustup update
```

Create the environment variable `MYSQLCLIENT_LIB_DIR`.
If mysql was installed with chocolatey the variable should probably point to `C:\tools\mysql\current\lib` where the file `mysqlclient.lib` is stored.

```
cargo install diesel_cli --no-default-features --features mysql
```

Create a virtual environment for python and install

```console
pip install virtualenv
python -m virtualenv venv
```

Activate the environment and install the dependencies

```console
source venv/Scripts/activate
pip install -r requirements.txt
```

Enable pre-commit

```console
pre-commit install
```

## Usage

### Docker

This project uses Docker extensively, in order to start everything do

```console
invoke docker
```

This will take a while the first time, but consecutive runs should be faster.

### Frontend

The frontend is a svelte application built with vite.
Start the development server with

```console
invoke frontend
```

### Backend

The backend is written in rust running serving an api via a websocket connection.

See the Docker section if you do not plan development on the backend.

Create `.env` file with database url, example for local developement:

```
DATABASE_URL=mysql://root:example@127.0.0.1:6677/mendia
```

If you want to develop the backend it is better to start it manually

```console
invoke backend
```

Make also sure to update the Caddyfile.
The hostname of the `/api/*` route points to the docker instance of the backend.
For local development change `mendia` to `host.docker.internal`

```
handle_path /api/* {
    reverse_proxy host.docker.internal:8000
}
```

Update the docker stack again with

```console
invoke docker
```

### Build the pipeline image

```console
docker login registry.gitlab.com
docker build -t registry.gitlab.com/derfreak/mendia:latest .
docker push registry.gitlab.com/derfreak/mendia:latest
```

### Upgrade packages

Install dependencies

```console
cargo install -f cargo-upgrades
cargo install -f cargo-edit
```

Upgrade packages

```console
cargo update
```

### Database

The database contains important data like users and movies.

##### Users

Create users or update the password of an existing user

```console
cd backend
cargo run --bin mendia-admin -- --config ./admin-config.yml --create-user USERNAME
```

##### Create/Update tables

Create tables via

```console
cd backend/types
diesel migration --database-url mysql://root:example@127.0.0.1:6677/mendia generate movies
```

Apply migration with

```console
cd backend/types
diesel migration --database-url mysql://root:example@127.0.0.1:6677/mendia run
```

Undo migration with

```console
cd backend/types
diesel migration --database-url mysql://root:example@127.0.0.1:6677/mendia redo
```

##### Testdata

This repository contains an example movie table.
It can be imported with docker:

```console
docker exec -i docker-db-1 mysql -uroot -pexample mendia < docker/data/mysql/movies.sql
```

## License

[Licensed under MIT](frontend/LICENSE)
