import { writable } from 'svelte/store';

export interface FrontendSession {
	username: string;
	apiKey: string;
	connection: WebSocket;
}

export const activeSession = writable<FrontendSession | undefined>(undefined);
