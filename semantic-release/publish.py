#! /usr/bin/env python
"""
    Publish the new version of the application to crates.io and npm.

    The script is run by semantic-release in the CI pipeline.
"""

import os
import sys
import subprocess
from subprocess import CompletedProcess
from typing import List

def do(args: List[str], cwd: str):
    result: CompletedProcess = subprocess.run(args, cwd=cwd)
    if result.returncode != 0:
        sys.exit(result.returncode)

def main() -> int:
    cargo_registry_token = os.getenv("CARGO_REGISTRY_TOKEN")

    print("Publish on crates.io")
    do(["cargo", "publish", "--token", cargo_registry_token, "-p", "mendia"], cwd="backend")

    print("Publish on npm")
    do(["npm", "publish"], cwd="frontend")

    return 0


if __name__ == "__main__":
    sys.exit(main())
