## [1.7.1](https://gitlab.com/derfreak/mendia/compare/v1.7.0...v1.7.1) (2025-01-19)


### Bug Fixes

* fix languages in telegram message ([6dc0b60](https://gitlab.com/derfreak/mendia/commit/6dc0b60e20d41faa871f81e02db18a63b32f8b5a))

# [1.7.0](https://gitlab.com/derfreak/mendia/compare/v1.6.3...v1.7.0) (2025-01-19)


### Features

* improved telegram message ([e4ff27c](https://gitlab.com/derfreak/mendia/commit/e4ff27c4cc499d85af9e3834fb2fd6c556a0377b)), closes [#16](https://gitlab.com/derfreak/mendia/issues/16)

## [1.6.3](https://gitlab.com/derfreak/mendia/compare/v1.6.2...v1.6.3) (2023-09-24)


### Bug Fixes

* fix login timing out ([5794b1e](https://gitlab.com/derfreak/mendia/commit/5794b1e3d21da0cbb1218c8f52e00faffc11f5ad))

## [1.6.2](https://gitlab.com/derfreak/mendia/compare/v1.6.1...v1.6.2) (2023-09-22)


### Bug Fixes

* fix movies added to db even if telegram message failed ([2697836](https://gitlab.com/derfreak/mendia/commit/26978361b0d7fd2af1010d0f9f2c61cf6d53b378))

## [1.6.1](https://gitlab.com/derfreak/mendia/compare/v1.6.0...v1.6.1) (2023-07-12)


### Bug Fixes

* remove hard coded backend path ([e193334](https://gitlab.com/derfreak/mendia/commit/e193334c655a18f2d4ae6c132a44216e4048f84d))

# [1.6.0](https://gitlab.com/derfreak/mendia/compare/v1.5.0...v1.6.0) (2023-06-04)


### Bug Fixes

* move global styles from component to main html ([9a6a705](https://gitlab.com/derfreak/mendia/commit/9a6a705f75794f0d902498afa6a84970e26a3e74))
* remove unused img tag ([9f696e5](https://gitlab.com/derfreak/mendia/commit/9f696e5c86b78a42cc3f6fe75828f9e129649247))


### Features

* layout with grids ([09162ff](https://gitlab.com/derfreak/mendia/commit/09162ff8d5aae7fabd25e02de5b491d1171662cc))

# [1.5.0](https://gitlab.com/derfreak/mendia/compare/v1.4.0...v1.5.0) (2022-12-30)


### Features

* provide tmdb api key via api ([f0cc307](https://gitlab.com/derfreak/mendia/commit/f0cc307c73f2cab562c19c31fb58048e43f0ef7a))

# [1.4.0](https://gitlab.com/derfreak/mendia/compare/v1.3.0...v1.4.0) (2022-12-25)


### Features

* publish new movies to telegram ([730e34c](https://gitlab.com/derfreak/mendia/commit/730e34c44b5af23a1f7a57def41cb223e19c2ea6))
* same config file for daemon and cli tool ([3d7499c](https://gitlab.com/derfreak/mendia/commit/3d7499cf8f3047af3dc7132f63ce7c130d63b0f2))

# [1.3.0](https://gitlab.com/derfreak/mendia/compare/v1.2.2...v1.3.0) (2022-12-22)


### Bug Fixes

* fix semantic release ([f526863](https://gitlab.com/derfreak/mendia/commit/f526863cb04451400c690e37243e051bb24f5051))
* version bump ([ef0f331](https://gitlab.com/derfreak/mendia/commit/ef0f331bfc9843084d9f079bc7a80b23d78c69b1))


### Features

* dummyfeat ([494aa71](https://gitlab.com/derfreak/mendia/commit/494aa7198dad2e0553e3677104adb519f73b69eb))

## [1.2.3](https://gitlab.com/derfreak/mendia/compare/v1.2.2...v1.2.3) (2022-12-22)


### Bug Fixes

* fix semantic release ([f526863](https://gitlab.com/derfreak/mendia/commit/f526863cb04451400c690e37243e051bb24f5051))
* version bump ([ef0f331](https://gitlab.com/derfreak/mendia/commit/ef0f331bfc9843084d9f079bc7a80b23d78c69b1))

## [1.2.2](https://gitlab.com/derfreak/mendia/compare/v1.2.1...v1.2.2) (2022-12-21)


### Bug Fixes

* npm publish ([6d59e37](https://gitlab.com/derfreak/mendia/commit/6d59e37646beedfc29dd4091ce37d865b1f3bddf))

## [1.2.1](https://gitlab.com/derfreak/mendia/compare/v1.2.0...v1.2.1) (2022-12-21)


### Bug Fixes

* npm publish ([62d4a9f](https://gitlab.com/derfreak/mendia/commit/62d4a9fa9a0fefab4d9631549c0bd8ee749a3183))

# [1.2.0](https://gitlab.com/derfreak/mendia/compare/v1.1.1...v1.2.0) (2022-12-21)


### Features

* show frontend version on login ([467f139](https://gitlab.com/derfreak/mendia/commit/467f13951577ae1cca2ce082fb529024b0313f23))

## [1.1.1](https://gitlab.com/derfreak/mendia/compare/v1.1.0...v1.1.1) (2022-12-21)


### Bug Fixes

* release on crates.io ([78fc26c](https://gitlab.com/derfreak/mendia/commit/78fc26c5a010ae0f56cc04840439def31953aeff))

# [1.1.0](https://gitlab.com/derfreak/mendia/compare/v1.0.0...v1.1.0) (2022-12-21)


### Bug Fixes

* release on crates.io ([0c30cc7](https://gitlab.com/derfreak/mendia/commit/0c30cc79e76cef2faa0ba47d264e87fbefecec89))


### Features

* notify with telegram that a user has new movies ([5a5615d](https://gitlab.com/derfreak/mendia/commit/5a5615d116aab139a68416f7d78bd63a59f1adea))

# 1.0.0 (2022-12-20)


### Bug Fixes

* missing index.html ([92ef2be](https://gitlab.com/derfreak/mendia/commit/92ef2bebb32984398b428b58d7618f9d3eb53dc9))
* remove eyesore coloring ([7d394ff](https://gitlab.com/derfreak/mendia/commit/7d394ff70db53825ae515aeabc9bc22ca5d9991b))


### Features

* accept new movies from users ([dd60bec](https://gitlab.com/derfreak/mendia/commit/dd60bec595483f5150f371e1d723fb1656a3787f)), closes [#10](https://gitlab.com/derfreak/mendia/issues/10)
* add hello world site ([96726d6](https://gitlab.com/derfreak/mendia/commit/96726d65b3be22d09b9c4d8986cd7aedb452a230))
* add telegram bot ([57f707d](https://gitlab.com/derfreak/mendia/commit/57f707d5d54e4c9ccb0b6f56c11dc8528abc65a7)), closes [#11](https://gitlab.com/derfreak/mendia/issues/11)
* create user with and store password safely ([0e8e4d8](https://gitlab.com/derfreak/mendia/commit/0e8e4d899300ca9ef2d54c00f01f0f1ab4780537))
* dummy movie titles from backend to frontend ([631208f](https://gitlab.com/derfreak/mendia/commit/631208fed6031e57f283256632fecf790e836d78))
* enable routes ([69f3890](https://gitlab.com/derfreak/mendia/commit/69f389098b57c669a6e4eed67124ede97fc171bd))
* Filter Movies by user ([5ecbc2d](https://gitlab.com/derfreak/mendia/commit/5ecbc2d51896d43044cba85c4315c6991f603578))
* full replace of react with svelte ([2076208](https://gitlab.com/derfreak/mendia/commit/2076208a42a7c033772f9415df28b73d6f2007c6)), closes [#6](https://gitlab.com/derfreak/mendia/issues/6)
* header ([a09ed3c](https://gitlab.com/derfreak/mendia/commit/a09ed3c422f3d8a6ae13fd963f14c5840749dfa0))
* list movies ([f992e59](https://gitlab.com/derfreak/mendia/commit/f992e59917c6ab73116908fbcadf36dd3729f13f))
* react, browserify and babel combined ([728c157](https://gitlab.com/derfreak/mendia/commit/728c1578ee88d842b7ca511c4d0a52438b25fd44))
* restyle login component ([ee31b66](https://gitlab.com/derfreak/mendia/commit/ee31b6626891bd60f7f6008f08767ee84761c5f1))
* restyle login site without bootsrap ([a439cae](https://gitlab.com/derfreak/mendia/commit/a439cae13f8bde3b609cb2a151209f84f5bea971))
* send password to backend and verify ([e41f66d](https://gitlab.com/derfreak/mendia/commit/e41f66dd3017c63fc33e7549dd92f51a3ac7d5f6))
* set header to 100% site width ([c73c5b7](https://gitlab.com/derfreak/mendia/commit/c73c5b72dc93b1461378596c2adaeffa1e178db2))
* styling ([fd78728](https://gitlab.com/derfreak/mendia/commit/fd787282fafcb697bc7825d1dd722f1cf7afb142))
* typescript from rust ([f82b4c3](https://gitlab.com/derfreak/mendia/commit/f82b4c33f89eaa354bafd81e344345f8b97b538a))
