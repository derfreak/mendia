-- Your SQL goes here

CREATE TABLE `movies` (
	`title` VARCHAR(320) NOT NULL COLLATE 'utf8mb4_general_ci',
	`year` INT(11) NOT NULL,
	`size` BIGINT(20) NOT NULL,
	`hash` VARCHAR(320) NOT NULL COLLATE 'utf8mb4_general_ci',
	`tmdb_id` BIGINT(20) NULL DEFAULT NULL,
	`audio_languages` VARCHAR(320) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
	`subtitle_languages` VARCHAR(320) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
	`resolution` VARCHAR(320) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
	`dynamic_range` VARCHAR(320) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
	`bitrate` BIGINT(20) NULL DEFAULT NULL,
	`user` VARCHAR(320) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
	PRIMARY KEY (`title`, `year`, `size`, `hash`) USING BTREE,
	UNIQUE INDEX `uid` (`title`, `year`, `size`, `hash`) USING BTREE
)
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
;
