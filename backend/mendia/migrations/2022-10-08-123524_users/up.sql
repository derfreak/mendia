-- Your SQL goes here

CREATE TABLE `users` (
	`name` VARCHAR(320) NOT NULL COLLATE 'utf8mb4_general_ci',
	`password_hash` VARCHAR(320) NOT NULL COLLATE 'utf8mb4_general_ci',
	PRIMARY KEY (`name`, `password_hash`) USING BTREE,
	UNIQUE INDEX `uid` (`name`, `password_hash`) USING BTREE
)
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
;
