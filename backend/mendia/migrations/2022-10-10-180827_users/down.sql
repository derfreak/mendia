-- This file should undo anything in `up.sql`

ALTER TABLE `users` DROP PRIMARY KEY;
ALTER TABLE `users` DROP INDEX `uid`;
ALTER TABLE `users` RENAME COLUMN `hash` TO `password_hash`;
ALTER TABLE `users` DROP COLUMN `salt`;
ALTER TABLE `users` ADD PRIMARY KEY (`name`, `password_hash`) USING BTREE;
ALTER TABLE `users` ADD UNIQUE INDEX `uid` (`name`, `password_hash`) USING BTREE;
