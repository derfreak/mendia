-- Your SQL goes here

ALTER TABLE `users` DROP PRIMARY KEY;
ALTER TABLE `users` DROP INDEX `uid`;
ALTER TABLE `users` RENAME COLUMN `password_hash` TO `hash`;
ALTER TABLE `users` ADD COLUMN `salt` VARCHAR(320) NOT NULL COLLATE 'utf8mb4_general_ci';
ALTER TABLE `users` ADD PRIMARY KEY (`name`) USING BTREE;
ALTER TABLE `users` ADD UNIQUE INDEX `uid` (`name`) USING BTREE;
