# mendia

## About

Mendia is a server application that keeps track of the movie collection of its users. Users can notify `mendia` about new movies in their collection and `mendia` can inform about new movies and their owner in a seperate telegram group.

> Note: For now mendia is constrained to telegram, this can change in the future.

The interface is websocket based and accepts json-formatted messages.

Users can:

- Log in
- Get a list of all movies
- Post a list of new movies

The [npm package mendia](https://www.npmjs.com/package/mendia) aims to implement this interface and provides a web based user interface for it.

## Usage

This package comes with two binaries:

- `mendia` - the main service running as a daemon
- `mendia-admin` - a cli tool for administrative tasks

> Note: Data is stored in a `mysql` compatible database which must be provided externally.

### Configuration

Both `mendia` and `mendia-admin` must be configured with a `*.yml` file that contains the database connection and settings for the telegram functionality.

> Note: `mendia-admin` only requires the database settings from the config file, `mendia` needs everything.

```yml
database:
  host: 127.0.0.1
  port: 3306
  database: mendia
  credentials:
    user: user
    password: example
websocket:
  host: '0.0.0.0'
  port: 8081
telegram:
  token: 'YOURBOTTOKEN'
  chatid: 'TARGETCHATID'
tmdb:
  api_key: 'YOURTHEMOVIEDBAPIKEY'
  language: 'en'
```

### Command Line Interface

`mendia-admin` is a cli-tool for administrative tasks.

#### Create a user or update an existing one

```console
mendia-admin --config config.yml --create-user martin
```

This will prompt for a password and stores it safely in the user table.

> Note: The password is hashed and salted with the argon2 algorithm.

### Daemon

The main binary `mendia` is a daemon-application that, once started, waits for new websocket connections and handles multiple users along each other.

Start the daemon

```console
mendia --config config.yml
```

### API

TODO this section must yet be written
