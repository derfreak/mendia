use std::{env, fs, net::SocketAddr};

use clap::Parser;

use color_eyre::eyre::{Context, Result};
use futures_channel::mpsc::unbounded;
use tokio::net::{TcpListener, TcpStream};

use futures_util::StreamExt;
use mendia::api::{MendiaReply, MendiaRequest, Session};

use mendia::{
    routes::{add_movies, login, movies},
    settings::Settings,
};

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    #[arg(long, short)]
    config: String,
}

#[tokio::main]
async fn main() -> Result<()> {
    color_eyre::install()?;

    let args = Args::parse();
    let settings: Settings = serde_yaml::from_str(
        &fs::read_to_string(&args.config)
            .wrap_err(format!("The file '{}' does not exist.", args.config))?,
    )
    .wrap_err("Failed to read settings from file.")?;

    let connection_string = format!("{}:{}", settings.websocket.host, settings.websocket.port);
    let listener = TcpListener::bind(connection_string.clone()).await?;

    // Set environment variable for teloxide telegram bot library.
    env::set_var("TELOXIDE_TOKEN", settings.telegram.token.clone());

    println!("Awaiting connections on ws://{}", connection_string);
    while let Ok((stream, addr)) = listener.accept().await {
        tokio::spawn(handle_connection(settings.clone(), stream, addr));
    }
    println!("Shutting down");
    Ok(())
}

async fn handle_connection(settings: Settings, stream: TcpStream, addr: SocketAddr) {
    let Ok(stream) = tokio_tungstenite::accept_async(stream).await else {
        eprintln!("Ignoring non WS connection from {addr}");
        return;
    };

    let (websocket_input, mut incoming) = stream.split();

    use tokio_tungstenite::tungstenite::Message::*;
    let init_msg = loop {
        let Some(Ok(init_msg)) = incoming.next().await else {
            eprintln!("Connection faulty!");
            return;
        };

        if let Text(txt) = init_msg {
            break txt;
        };
    };

    let Ok(MendiaRequest::LoginCredentials(credentials)) =
        serde_json::from_str::<MendiaRequest>(&init_msg)
    else {
        eprintln!("Faiiled to parse login credentials from string");
        return;
    };

    let (outgoing, to_websocket) = unbounded();
    // Spawn task to forward all messages from the internal channel to the websocket.
    // It is important to do this before trying to log in, so any potential login failed messages
    // are also forwarded.
    tokio::spawn(
        to_websocket
            .map(|msg| serde_json::to_string(&msg))
            // serialization here should not fail
            .map(|msg| {
                Ok(Text(
                    msg.expect("Failed to serialize message to json").into(),
                ))
            })
            .forward(websocket_input),
    );

    let Ok(api_key) = login(&settings, &credentials).await else {
        eprintln!(
            "User {} failed to login, closing connection.",
            credentials.username
        );
        return;
    };

    println!("Starting connection with '{}'.", credentials.username);

    let msg = MendiaReply::Session(Session {
        username: credentials.username.clone(),
        api_key: api_key.clone(),
    });
    outgoing
        .unbounded_send(msg)
        .expect("Failed to send a message.");

    while let Some(msg) = incoming.next().await {
        let Ok(Text(txt)) = msg else {
            continue;
        };
        // JSON received

        // Deserialize
        let data = serde_json::from_str(&txt);
        let data = match data {
            Ok(data) => data,
            Err(err) => {
                outgoing
                    .unbounded_send(MendiaReply::InvalidData(InvalidData {
                        data: txt.to_string(),
                        error_msg: err.to_string(),
                    }))
                    .expect("Failed to send message.");
                continue;
            } // Send error message back
        };

        use mendia::api::*;
        use MendiaRequest::*;

        match data {
            LoginCredentials(_) => continue,
            GetMovies { user: _ } => {
                let movies = movies(&settings, api_key.clone()).await;
                outgoing
                    .unbounded_send(MendiaReply::Movies { movies })
                    .expect("Failed to send message.");
            }
            PushMovies(push_cmd) => {
                // TODO This should be tested once for all authorized commands.
                // For now we do this here as the PushMovies command must not be made public.
                if push_cmd.username != credentials.username || push_cmd.api_key != api_key {
                    outgoing
                        .unbounded_send(MendiaReply::PushMoviesResult(PushMoviesResult {
                            success: false,
                            reason: "Credential mismatch, abort.".to_owned(),
                        }))
                        .expect("Failed to send message.");
                    return;
                }
                add_movies(&settings, credentials.username.clone(), &push_cmd.movies).await;
                outgoing
                    .unbounded_send(MendiaReply::PushMoviesResult(PushMoviesResult {
                        success: true,
                        reason: "".to_owned(),
                    }))
                    .expect("Failed to send message.");
            }
            GetTMDbApiKey(cmd) => {
                if cmd.username != credentials.username || cmd.api_key != api_key {
                    outgoing
                        .unbounded_send(MendiaReply::GetTMDbApiKeyResult(GetTMDbApiKeyResult {
                            tmdb_api_key: None,
                        }))
                        .expect("Failed to send message.");
                    return;
                }
                outgoing
                    .unbounded_send(MendiaReply::GetTMDbApiKeyResult(GetTMDbApiKeyResult {
                        tmdb_api_key: Some(settings.tmdb.api_key.clone()),
                    }))
                    .expect("Failed to send message.");
            }
        }
    }
    println!("Ending connection with '{}'.", credentials.username);
}
