use color_eyre::Result;
use diesel::prelude::*;
use rand::distributions::Alphanumeric;
use rand::{thread_rng, Rng};
use teloxide::types::InputFile;

use crate::api::LoginCredentials;
use crate::db::{connect, Movie, User};

use crate::settings::{Settings, Telegram, TheMovieDb};
use crate::tmdb::poster_url;

pub async fn movies(settings: &Settings, _api_key: String) -> Vec<Movie> {
    use crate::schema::movies::dsl::*;
    if let Ok(mut connection) = connect(&settings.database).await {
        if let Ok(result) = movies.load::<Movie>(&mut connection) {
            return result;
        }
    }
    Vec::<Movie>::new()
}

async fn create_new_movie_text(movie: &Movie) -> String {
    let size = movie.size as f64 / 1e9;
    let bitrate = match movie.bitrate {
        Some(bitrate) => bitrate as f64 / 1e6,
        _ => -1.0,
    };
    let s = |s: &Option<String>| match &s {
        Some(a) => a.clone(),
        _ => String::new(),
    };
    let not_found_text = "Keine erkannt";
    let print_language = |input: &Option<String>| match input {
        Some(l) => {
            let parts: Vec<_> = l.split(',').collect();
            if parts.is_empty() {
                return not_found_text.to_owned();
            }
            parts
                .into_iter()
                .map(|language| {
                    let language = language.trim().to_lowercase();
                    if let Some(full) = rust_iso639::from_code_1(&language) {
                        let full = full.name;
                        return format!("  - {full}");
                    }
                    if let Some(full) = rust_iso639::from_code_2t(&language) {
                        let full = full.name;
                        return format!("  - {full}");
                    }
                    if let Some(full) = rust_iso639::from_code_2b(&language) {
                        let full = full.name;
                        return format!("  - {full}");
                    }
                    format!("  - {language}")
                })
                .collect::<Vec<_>>()
                .join("\n")
        }
        _ => not_found_text.to_owned(),
    };
    format!(
        "{}{} ({})

{} {}
{:.2} GB - {:.2} Mb/s

Tonspuren:
{}
Untertitel:
{}
",
        if let Some(user) = &movie.user {
            format!("{user} hat nun\n")
        } else {
            "".into()
        },
        movie.title,
        movie.year,
        s(&movie.resolution),
        s(&movie.dynamic_range),
        size,
        bitrate,
        print_language(&movie.audio_languages),
        print_language(&movie.subtitle_languages),
    )
}

async fn publish_movies(
    telegram_settings: &Telegram,
    tmdb_settings: &TheMovieDb,
    movies: &[&Movie],
) {
    use teloxide::prelude::*;

    let bot = Bot::from_env();

    for movie in movies {
        let Some(tmdb_id) = movie.tmdb_id else {
            continue;
        };

        let poster_url = match poster_url(tmdb_settings, tmdb_id as u64).await {
            Ok(url) => url,
            Err(msg) => {
                eprintln!("{}", msg);
                continue;
            }
        };

        let msg = create_new_movie_text(movie).await;

        // For channels we have to prepend -100 to the actual id.
        // https://stackoverflow.com/questions/71275683/how-to-have-telegram-bot-post-messages-to-my-channel-and-not-its-own-channel
        bot.send_photo(telegram_settings.chatid.clone(), InputFile::url(poster_url))
            .caption(msg)
            .await
            .unwrap_or_else(|_| panic!("Failed to publish '{} ({}).'", movie.title, movie.year));
    }
}

pub async fn add_movies(settings: &Settings, username: String, movie_list: &[Movie]) {
    let fixed_username_movies: Vec<_> = movie_list
        .iter()
        .map(|movie| {
            let mut fixed = movie.clone();
            fixed.user = Some(username.clone());
            fixed
        })
        .collect();

    let Ok(mut connection) = connect(&settings.database).await else {
        return;
    };

    let mut new_movies = Vec::new();

    use crate::schema::movies::dsl::*;
    for movie in movie_list {
        let Ok(count) = movies
            .filter(tmdb_id.eq(movie.tmdb_id))
            .count()
            .get_result::<i64>(&mut connection)
        else {
            continue;
        };
        if count > 0 {
            continue;
        }
        new_movies.push(movie);
    }

    publish_movies(&settings.telegram, &settings.tmdb, &new_movies).await;

    diesel::insert_or_ignore_into(crate::schema::movies::table)
        .values(fixed_username_movies)
        .execute(&mut connection)
        .expect("Failed to insert movies");
}

pub async fn login(settings: &Settings, credentials: &LoginCredentials) -> Result<String> {
    use crate::schema::users::dsl::*;
    use argon2::Config;

    let Ok(mut connection) = connect(&settings.database).await else {
        return Err(color_eyre::eyre::eyre!(
            "Failed to connect to the database via {:?}",
            settings.database,
        ));
    };

    let results: Vec<User> = users
        .filter(name.eq(credentials.username.clone()))
        .load::<User>(&mut connection)?;

    if results.is_empty() {
        return Err(color_eyre::eyre::eyre!(
            "Username not {:?} found",
            credentials.username
        ));
    }

    let user = &results[0];
    let hash_value = argon2::hash_encoded(
        credentials.password.as_bytes(),
        user.salt.as_bytes(),
        &Config::original(),
    )?;

    let valid_hash = argon2::verify_encoded(&hash_value, credentials.password.as_bytes())?;

    if !valid_hash || hash_value != user.hash {
        return Err(color_eyre::eyre::eyre!("Invalid password"));
    }

    // https://rust-lang-nursery.github.io/rust-cookbook/algorithms/randomness.html#create-random-passwords-from-a-set-of-alphanumeric-characters
    let random_api_key: String = thread_rng()
        .sample_iter(&Alphanumeric)
        .take(30)
        .map(char::from)
        .collect();
    Ok(random_api_key)
}

#[cfg(test)]
mod tests {
    use teloxide::prelude::*;

    use crate::{db::Movie, routes::create_new_movie_text};

    #[tokio::test]
    async fn publish_telegram_message() {
        let bot = Bot::from_env();

        let _ = bot
            .send_message("-1001906511632".to_owned(), "Test publish_telegram_message")
            .await
            .unwrap();
    }

    #[tokio::test]
    async fn movie_text() {
        let movie = Movie {
            title: "Test Movie".into(),
            year: 1942,
            size: 7000000000,
            hash: "d41d8cd98f00b204e9800998ecf8427e".into(),
            tmdb_id: Some(4242),
            audio_languages: Some("de,en, ara  ,fr".into()),
            subtitle_languages: Some("en,de,fr,chi".into()),
            resolution: Some("1920x1080".into()),
            dynamic_range: Some("HDR".into()),
            bitrate: Some(5000000),
            user: Some("pablita".into()),
        };
        let text = create_new_movie_text(&movie).await;
        assert_eq!(
            text,
            "pablita hat nun
Test Movie (1942)

1920x1080 HDR
7.00 GB - 5.00 Mb/s

Tonspuren:
  - German
  - English
  - Arabic
  - French
Untertitel:
  - English
  - German
  - French
  - Chinese
"
        );
    }
}
