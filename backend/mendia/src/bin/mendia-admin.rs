use std::{convert::From, fs};

use clap::Parser;
use color_eyre::eyre::{eyre, Context, Result};
use diesel::mysql::MysqlConnection;
use diesel::prelude::*;

use mendia::db::connect;
use mendia::settings::Endpoint;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct Settings {
    pub database: Endpoint,
}

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    #[arg(long)]
    create_user: Option<String>,

    #[arg(long)]
    config: String,
}

fn create_user(connection: &mut MysqlConnection, user_name: &String) -> Result<()> {
    use argon2::Config;
    use mendia::{db::User, schema::users};
    use rand::{distributions::Alphanumeric, Rng};

    let random_generator = rand::thread_rng();

    let password = inquire::Password::new("Enter a safe password")
        .with_display_mode(inquire::PasswordDisplayMode::Masked)
        .with_validator(inquire::validator::MinLengthValidator::new(8))
        .prompt()
        .wrap_err("Failed to inquire the user password.")?;

    let salt = random_generator
        .sample_iter(&Alphanumeric)
        .take(64)
        .map(char::from)
        .collect::<String>();

    let hash = argon2::hash_encoded(password.as_bytes(), salt.as_bytes(), &Config::original())
        .wrap_err("Failed to hash the password.")?;

    let Ok(true) = argon2::verify_encoded(&hash, password.as_bytes()) else {
        return Err(eyre!("Failed to verify the hashed password."));
    };

    let new_user = User {
        name: user_name.to_string(),
        hash,
        salt,
    };

    diesel::replace_into(users::table)
        .values(&new_user)
        .execute(connection)
        .wrap_err("Failed to create/update the usertable.")?;

    Ok(())
}

#[tokio::main]
async fn main() -> Result<()> {
    color_eyre::install()?;

    let args = Args::parse();

    let endpoint = serde_yaml::from_str::<Settings>(
        &fs::read_to_string(&args.config)
            .wrap_err(format!("The file '{}' does not exist.", args.config))?,
    )
    .wrap_err("Failed to read settings from file.")?
    .database;

    let mut connection = connect(&endpoint)
        .await
        .wrap_err("Failed to connect to the database.")?;

    if let Some(user_name) = args.create_user {
        create_user(&mut connection, &user_name)?;
    }

    Ok(())
}
