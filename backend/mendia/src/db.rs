use diesel::{prelude::*, Connection, ConnectionResult, MysqlConnection};
use serde::{Deserialize, Serialize};
use ts_rs::TS;

use crate::schema;
use crate::settings::Endpoint;

#[derive(TS, Serialize, Deserialize, Insertable, Queryable, Debug, Clone)]
#[diesel(table_name = schema::movies)]
#[ts(export)]
pub struct Movie {
    pub title: String,
    pub year: i32,
    pub size: i64,
    pub hash: String,
    pub tmdb_id: Option<i64>,
    pub audio_languages: Option<String>,
    pub subtitle_languages: Option<String>,
    pub resolution: Option<String>,
    pub dynamic_range: Option<String>,
    pub bitrate: Option<i64>,
    pub user: Option<String>,
}

#[derive(Insertable, Queryable)]
#[diesel(table_name = schema::users)]
pub struct User {
    pub name: String,
    pub hash: String,
    pub salt: String,
}

pub async fn connect(endpoint: &Endpoint) -> ConnectionResult<MysqlConnection> {
    let url = format!(
        "mysql://{}:{}@{}:{}/{}",
        endpoint.credentials.user,
        endpoint.credentials.password,
        endpoint.host,
        endpoint.port,
        endpoint.database,
    );
    MysqlConnection::establish(&url)
}
