// @generated automatically by Diesel CLI.

diesel::table! {
    movies (title, year, size, hash) {
        title -> Varchar,
        year -> Integer,
        size -> Bigint,
        hash -> Varchar,
        tmdb_id -> Nullable<Bigint>,
        audio_languages -> Nullable<Varchar>,
        subtitle_languages -> Nullable<Varchar>,
        resolution -> Nullable<Varchar>,
        dynamic_range -> Nullable<Varchar>,
        bitrate -> Nullable<Bigint>,
        user -> Nullable<Varchar>,
    }
}

diesel::table! {
    users (name) {
        name -> Varchar,
        hash -> Varchar,
        salt -> Varchar,
    }
}

diesel::allow_tables_to_appear_in_same_query!(movies, users,);
