use serde::{Deserialize, Serialize};
use ts_rs::TS;

use crate::db;

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[ts(export)]
pub struct LoginCredentials {
    pub username: String,
    pub password: String,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[ts(export)]
pub struct PushMovies {
    pub username: String,
    pub api_key: String,
    pub movies: Vec<db::Movie>,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[ts(export)]
pub struct GetTMDbApiKey {
    pub username: String,
    pub api_key: String,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[ts(export)]
pub struct GetTMDbApiKeyResult {
    pub tmdb_api_key: Option<String>,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[ts(export)]
pub struct Session {
    pub username: String,
    pub api_key: String,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[ts(export)]
pub struct InvalidData {
    pub data: String,
    pub error_msg: String,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[ts(export)]
pub struct LoginFailed {
    pub reason: String,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[ts(export)]
pub struct PushMoviesResult {
    pub success: bool,
    pub reason: String,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(tag = "type")]
#[ts(export)]
/// Event sent from backend to frontend
pub enum MendiaReply {
    NotImplemented { command: String },
    InvalidData(InvalidData),
    LoginFailed(LoginFailed),
    Session(Session),
    Movies { movies: Vec<db::Movie> },
    PushMoviesResult(PushMoviesResult),
    GetTMDbApiKeyResult(GetTMDbApiKeyResult),
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(tag = "type")]
#[ts(export)]
/// Event sent from frontend to backend
pub enum MendiaRequest {
    LoginCredentials(LoginCredentials),
    GetMovies { user: String },
    PushMovies(PushMovies),
    GetTMDbApiKey(GetTMDbApiKey),
}
